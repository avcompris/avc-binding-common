# About avc-binding-common

This project contains common classes shared by Avantage Compris' projects for Java binding.

Its parent project is [avc-commons-parent](https://gitlab.com/avcompris/avc-commons-parent/).

Project dependencies include:

  * [avc-commons-lang](https://gitlab.com/avcompris/avc-commons-lang/)
  * [avc-commons-testutil](https://gitlab.com/avcompris/avc-commons-testutil/)

Projects which depend on this one include:

  * [avc-binding-dom](https://gitlab.com/avcompris/avc-binding-dom/)
  * [avc-binding-yaml](https://gitlab.com/avcompris/avc-binding-yaml/)

This is the project home page, hosted on GitLab.

[API Documentation is here](https://maven.avcompris.com/avc-binding-common/apidocs/index.html).

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-binding-common/)
