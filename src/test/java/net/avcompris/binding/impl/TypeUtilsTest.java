package net.avcompris.binding.impl;

import static org.joda.time.DateTimeZone.UTC;
import static org.junit.Assert.assertEquals;

import org.joda.time.DateTime;
import org.junit.Test;

public class TypeUtilsTest {

	@Test
	public void testUnmarshallDateTimeUTC() throws Exception {

		assertEquals(new DateTime(2010, 10, 1, 2, 3, 4, 5, UTC),
				TypeUtils.unmarshallValue(DateTime.class,
						"2010-10-01T02:03:04.005Z"));
	}
}
