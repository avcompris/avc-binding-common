package net.avcompris.binding;

import static org.mockito.Mockito.mock;

import java.util.Collection;

import net.avcompris.binding.impl.AbstractBinder;
import net.avcompris.binding.impl.AbstractBinderInvocationHandler;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import com.avcompris.common.annotation.Nullable;
import com.avcompris.util.junit.AvcParameterized;
import com.avcompris.util.reflect.AbstractNonNullTest;
import com.avcompris.util.reflect.UseInstance;

/**
 * tests on Java classes in the "<code>net.avcompris.binding</code>" package, to check
 * if the method parameters that are not flagged
 * as {@link Nullable} are checked for <code>null</code>-values.
 * 
 * @author David Andrianavalontsalama 2011 ©
 */
@RunWith(AvcParameterized.class)
public class AvcBindingCommonNonNullTest extends AbstractNonNullTest {

	/**
	 * constructor.
	 * 
	 * @param holder the instance+member to test
	 */
	public AvcBindingCommonNonNullTest(final MemberHolder holder)
			throws Exception {

		super(holder);
	}

	/**
	 * @return all the Java methods to test.
	 */
	@Parameters
	public static Collection<?> parameters() throws Exception {

		return parametersScanProject(BindConfiguration.newBuilder(),
				BindConfiguration.newBuilder().build(), BindingException.class,
				AbstractBinder.class, AbstractBinderInvocationHandler.class,
				new ClassBinding(null, "*"), new MethodBinding(null, "*"),
				UseInstance.forClass(Binder.class, mock(AbstractBinder.class),
						false));
	}
}
