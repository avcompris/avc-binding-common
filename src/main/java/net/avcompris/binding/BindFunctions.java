package net.avcompris.binding;

/**
 * implement this interface and use your custom XPath functions.
 * 
 * @author David Andrianavalontsalama
 */
public interface BindFunctions {

	/**
	 * return <code>true</code> if the XPath functions implemented by this object
	 * are to be evaluated in the given namespace.
	 */
	boolean matchesNamespaceURI(String namespaceURI);
}
