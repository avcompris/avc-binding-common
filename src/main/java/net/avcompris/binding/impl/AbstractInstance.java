package net.avcompris.binding.impl;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;

abstract class AbstractInstance<T> {

	final void setInstance(final T instance) {

		this.instance = nonNullArgument(instance, "instance");
	}

	final T getInstance() {

		if (instance == null) {
			throw new IllegalStateException(
					"Internal instance has not been set.");
		}

		return instance;
	}

	private T instance;
}
