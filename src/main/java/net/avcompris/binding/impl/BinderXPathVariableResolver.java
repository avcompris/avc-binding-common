package net.avcompris.binding.impl;

import javax.xml.namespace.QName;

/**
 * used for instance for bridges between DOM and avc-binding-common.
 *
 * @author David Andrianavalontsalama
 */
public interface BinderXPathVariableResolver {

	void setThisNode(Object node);
	
	Object resolveVariable(QName variableName);
}
