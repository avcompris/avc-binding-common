package net.avcompris.binding.impl;

import com.avcompris.common.annotation.Nullable;

public interface XPathFunction {

	boolean matchesArity(int arity);

	Object evaluate(@Nullable final Object[] args);
}
