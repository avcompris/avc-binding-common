package net.avcompris.binding.impl;

public interface XPathFunctionResolver {

	XPathFunction resolveDomFunction(String namespaceURI, String localPart);
}
