package net.avcompris.binding.impl;

import java.util.Map;

public interface Instance<U> {

	Map<String, Object> getAttributes(U node);

	void setAttribute(U node, String name, Object value);

	void setAttribute(U attributeNode, Object value);

	void removeAttribute(U node, String name);

	Iterable<U> getChildren(U node);

	void remove(U node);
}
