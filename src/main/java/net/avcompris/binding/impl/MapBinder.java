package net.avcompris.binding.impl;

interface MapBinder {

	/**
	 * bind an element in the map.
	 */
	void bind(Object key, Object value);
}
