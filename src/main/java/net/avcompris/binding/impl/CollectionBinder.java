package net.avcompris.binding.impl;

interface CollectionBinder {

	/**
	 * bind an element in the collection.
	 */
	void bind(int index, Object value);
}
