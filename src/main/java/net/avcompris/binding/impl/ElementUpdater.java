package net.avcompris.binding.impl;

import java.util.Map;

public interface ElementUpdater<U> {

	U getParent(U node);
	
	String getName(U node);
	
	void setName(U node, String name);
	
	Map<String, Object> getAttributes(U node);

	void setAttribute(U node, String name, Object value);

	void setAttribute(U attributeNode, Object value);

	void removeAttribute(U node, String name);

	void setNode(U node, Object value);
	
	Iterable<U> getChildren(U node);

	U addToChildren(U node, String name);

	void remove(U node);

	void setValue(U node, Object value);
}
