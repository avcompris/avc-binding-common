package net.avcompris.binding.impl;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;

import java.util.Map;

import com.avcompris.common.annotation.Nullable;
import com.avcompris.lang.NotImplementedException;

/**
 * internal empty updater for non-Document Binder implementations, such as 
 * those mocking DOM binding using SAX.
 * 
 * @author David Andrianavalontsalama
 */
final class DummyInstanceUpdater<U> extends InstanceUpdater<U> {

	public DummyInstanceUpdater(final Object instance) {

		nonNullArgument(instance, "instance");
	}

	@Override
	public Map<String, Object> getAttributes(final U node) {

		nonNullArgument(node, "node");

		throw new NotImplementedException();
	}

	@Override
	public Iterable<U> getChildren(final U node) {

		nonNullArgument(node, "node");

		throw new NotImplementedException();
	}

	@Override
	public void remove(final U node) {

		nonNullArgument(node, "node");

		throw new NotImplementedException();
	}

	@Override
	public void removeAttribute(final U node, final String name) {

		nonNullArgument(node, "node");
		nonNullArgument(name, "name");

		throw new NotImplementedException();
	}

	@Override
	public void setAttribute(final U node, final String name,
			@Nullable final Object value) {

		nonNullArgument(node, "node");
		nonNullArgument(name, "name");

		throw new NotImplementedException();
	}

	@Override
	public void setAttribute(final U attributeNode, @Nullable final Object value) {

		nonNullArgument(attributeNode, "attributeNode");

		throw new NotImplementedException();
	}

	@Override
	public void setNode(final U node, @Nullable final Object value) {

		nonNullArgument(node, "node");

		throw new NotImplementedException();
	}

	@Override
	public U addToChildren(final U node, final String name) {

		nonNullArgument(node, "node");
		nonNullArgument(name, "name");

		throw new NotImplementedException();
	}

	@Override
	public void setValue(final U node, final Object value) {

		nonNullArgument(node, "node");
		nonNullArgument(value, "value");

		throw new NotImplementedException();
	}

	@Override
	public U getParent(final U node) {

		nonNullArgument(node, "node");

		throw new NotImplementedException();
	}

	@Override
	public String getName(final U node) {

		nonNullArgument(node, "node");

		throw new NotImplementedException();
	}

	@Override
	public void setName(final U node, final String name) {

		nonNullArgument(node, "node");
		nonNullArgument(name, "name");

		throw new NotImplementedException();
	}
}
