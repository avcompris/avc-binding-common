package net.avcompris.binding.impl;

import net.avcompris.binding.annotation.XPath;

/**
 * dummy interface to retrieve simple values. 
 */
interface Dummy {

	@XPath(".")
	String getStringValue();

	@XPath(".")
	char getCharValue();

	@XPath(".")
	boolean getBooleanValue();

	@XPath(".")
	byte getByteValue();

	@XPath(".")
	int getIntValue();

	@XPath(".")
	short getShortValue();

	@XPath(".")
	long getLongValue();

	@XPath(".")
	float getFloatValue();

	@XPath(".")
	double getDoubleValue();
}
