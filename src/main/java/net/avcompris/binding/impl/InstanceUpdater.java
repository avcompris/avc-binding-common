package net.avcompris.binding.impl;


abstract class InstanceUpdater<U> extends AbstractInstance<Object> implements
		ElementUpdater<U> {

	// empty class
}
