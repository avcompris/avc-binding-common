package net.avcompris.binding.impl;

import net.avcompris.binding.Serializer;

public abstract class AbstractSerializer<T> implements Serializer<T> {

	// empty class
}
