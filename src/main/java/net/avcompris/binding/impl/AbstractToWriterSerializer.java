package net.avcompris.binding.impl;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;

import java.io.Writer;

public abstract class AbstractToWriterSerializer<T> extends AbstractSerializer<T> {

	protected AbstractToWriterSerializer(final Writer writer) {

		this.writer = nonNullArgument(writer, "writer");
	}

	protected final Writer writer;
}
