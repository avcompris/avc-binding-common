package net.avcompris.binding;

import java.io.IOException;

/**
 * serializer flag.
 * 
 * @author David Andrianavalontsalama
 */
public interface Serializer<T> {

	/**
	 * serialize a node of type <code>T</code>,
	 * for instance <code>org.w3c.dom.Node</code>.
	 */
	void serialize(T node) throws IOException;
}
