package net.avcompris.binding;

import com.avcompris.common.annotation.Nullable;

/**
 * This exception will be thrown when a call to a method bound
 * should encounter an error due to an XPath expression. 
 */
public class BindingException extends RuntimeException {

	/**
	 * for serialization.
	 */
	private static final long serialVersionUID = -5962681573428846346L;

	/**
	 * return the XPath expression.
	 */
	public String getXPath() {

		return xpath;
	}

	/**
	 * constructor.
	 */
	public BindingException(
			@Nullable final String xpath,
			@Nullable final Throwable cause) {

		super("Cannot compute XPath expression: " + xpath, cause);

		this.xpath = xpath;
	}

	private final String xpath;
}
