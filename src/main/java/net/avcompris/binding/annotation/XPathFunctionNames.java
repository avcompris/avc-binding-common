package net.avcompris.binding.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * use this annotation to map Java methods to XPath function name(s), when the
 * default behaviour of taking the name of the Java method as the XPath function
 * name is not enough.
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface XPathFunctionNames {

	String[] value() default {};
}
