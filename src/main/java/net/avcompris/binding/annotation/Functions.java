package net.avcompris.binding.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.BindFunctions;

/**
 * annotate binding Java interfaces with custom XPath function declarations
 * so XPath expressions can be interpreted.
 *
 * @author David Andrianavalontsalama
 */
@SuppressWarnings("unused")
@Documented
@Target(TYPE)
@Retention(RUNTIME)
public @interface Functions {

	/**
	 * the classes in which the custom XPath functions are implemented.
	 * Use {@link BindConfiguration#addFunctions(Class, Functions)} if you want a
	 * more precise behaviour.
	 */
	Class<? extends BindFunctions>[] value() default {};
}
