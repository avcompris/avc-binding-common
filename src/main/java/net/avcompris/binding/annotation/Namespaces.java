package net.avcompris.binding.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * annotate binding Java interfaces with namespace declarations
 * so XPath expressions can be interpreted.
 * 
 * @author David Andrianavalontsalama
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface Namespaces {

	/**
	 * the namespaces, as key/value pairs, or as expressions. They may be
	 * mixed. Examples:
	 * <ul>
	 * <li><code>@Namespaces({"pom", "http://maven.apache.org/POM/4.0.0"})</code>
	 * <li><code>@Namespaces({"pom", "http://maven.apache.org/POM/4.0.0", "xsi", "http://www.w3.org/2001/XMLSchema-instance"})</code>
	 * <li><code>@Namespaces("xmlns:pom=http://maven.apache.org/POM/4.0.0")</code>
	 * <li><code>@Namespaces({"xmlns:pom=http://maven.apache.org/POM/4.0.0", "xsi", "http://www.w3.org/2001/XMLSchema-instance"})</code>
	 * </ul>
	 */
	String[] value();
}
