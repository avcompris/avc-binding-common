package net.avcompris.binding;

public interface BinderFilter<U> extends Binder<U> {

	Binder<U> getDelegate();
}
